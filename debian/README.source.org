* Setup packaging environment

To start working on nqp:

** install development packages

#+BEGIN_SRC shell
sudo apt install git-buildpackage devscripts cme pkg-perl-tools
#+END_SRC

** set up your environement

With something like in =~/.devscripts=

#+begin_example
DEBCHANGE_RELEASE_HEURISTIC=changelog
DEBUILD_LINTIAN=yes
DEBUILD_LINTIAN_OPTS="-i -I --show-overrides"
USCAN_DESTDIR=../tarballs
DEBRELEASE_DEBS_DIR=../build-area
#+end_example

In =~/.gbp.conf=:

#+begin_example
[DEFAULT]
pristine-tar = True
sign-tags = True
[buildpackage]
# use a build area relative to the git repository
export-dir=../build-area/
tarball-dir = ../tarballs/
urgency=low
# to use the same build area for all packages use an absolute path:
#export-dir=/home/debian-packages/build-area
[import-orig]
# run hook after the import:
postimport = gbp dch -N%(version)s -S -a --debian-branch=$GBP_BRANCH
#+end_example

** Setup work directory

Clone nqp
#+BEGIN_SRC 
gbp clone git+ssh://git.debian.org/git/pkg-rakudo/nqp.git
#+END_SRC

** Upgrade package

Import new version (and reply to questions):
#+BEGIN_SRC shell :dir ..
gbp import-orig --uscan
#+END_SRC

#+RESULTS:

Update debian/control for moarvm version:
#+BEGIN_SRC shell :dir .. :results verbatim
perl debian/update-control.pl
#+END_SRC

Check for issues:
#+BEGIN_SRC shell :dir .. :results verbatim
cme check dpkg
#+END_SRC

Optionally, let =cme= fix mistakes:
#+BEGIN_SRC shell :dir .. :results verbatim
cme fix dpkg
#+END_SRC

Update copyright (be sure to commit copyright file before):
#+BEGIN_SRC shell :dir .. :results verbatim
cme update dpkg-copyright
#+END_SRC

To build the package:
#+BEGIN_SRC shell :dir .. :results verbatim
gbp buildpackage -us -uc
#+END_SRC


To cleanup cruft (dangerous, commit your work before):
#+BEGIN_SRC
git reset --hard
git clean -dxf
#+END_SRC


  
Push the update to Salsa:
#+BEGIN_SRC shell :dir ..
gbp push
#+END_SRC


** Notes about the source:

*** Jar files

Upstream nqp is delivered with a set of jar files without source.
These jar files are removed when upstream tarball is imported
(see [[file:copyright][Debian/copyright]] file).

There's no need to worry about the jar files as long as we don't
provide nqp/jvm.

The jar files in stage0 directory can be rebuilt with:

#+BEGIN_SRC 
configure && make && make m-bootstrap-files
#+END_SRC

*** UCD data files

Upstream nqp also delivers UCD data file in
src/vm/js/nqp-runtime/unicode-data/ (seen from version 2018.01). The
source of these files are from Unicode Character Database. These
files are also provided by Debian unicode-data package, but no
provision is given (yet) to rebuild these files from UCD data (See
https://github.com/perl6/nqp/issues/407). Since js backend is not
built for Debian, src/vm/js directory is removed from upstream (see
debian/copyright file)

*** src/vm/moar/stage0/ directory

This directory contains binary files. These files are a pre-compiled
nqp compiler and are necessary to compile nqp. nqp is a bootstrapping
compiler that uses itself to compile nqp. See file
docs/bootstrapping.pod for more details

*** profiler/template.html

This file is not shipped. This file loads script from internet which
leads to privacy breach warnings from lintian. =template.html= is part
of profiling tool. I expect people involved in profiling to work with
upstream nqp and not with Debian package. I'll revert this if I'm
proven wrong.

